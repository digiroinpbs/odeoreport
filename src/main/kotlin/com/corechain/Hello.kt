package com.corechain
import com.google.gson.*
import com.rethinkdb.RethinkDB
import com.rethinkdb.gen.exc.ReqlError
import com.rethinkdb.net.Connection
import com.rethinkdb.net.Cursor
import spark.Spark.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

fun main(args: Array<String>) {
    port(8001)
    val odeo_mid = "4465"
    val odeo_secret_key = "$2y$10\$nTa6j.hFC788hNTP962HHO0OeLgXaVIlmTT9nQYxTyPyUvF8RCUNy"
    val odeo_endpoint_purchase: String = "https://private-anon-e0a595e5b3-ppob.apiary-mock.com/v1/affiliate/prepaid/purchase"
    val odeo_endpoint_notify:String = "https://api.odeo.co.id/affiliate_url"
    val key = odeo_mid + ':' + odeo_secret_key
    val token = Base64.getEncoder().encodeToString(key.toByteArray())

    post("/reportodeo",fun(req,res):JsonElement{
        var results:String = ""
        var parser:JsonParser = JsonParser()

        try {
            val r: RethinkDB = RethinkDB();
            val conn: Connection = r.connection().hostname("rth0.corechain.id").port(28015).db("dev").user("digiro", "digiro").connect()
            var startDate = req.queryParams("startDate")
            var endDate = req.queryParams("endDate")
            var unixStartDate = convertToUnixDate(startDate)
            var unixEndDate = convertToUnixDate(endDate)

            var cursor:Cursor<HashMap<String,String>> = r.table("odeo_trx")
            .filter(
                    { x -> x.g("process_date").le(unixEndDate) }
            ).filter(
                    { x -> x.g("process_date").ge(unixStartDate) }
            )
             .run(conn)

            var gson = GsonBuilder().disableHtmlEscaping().setLenient().create()
            var itr:Int = 0
            var str:String = ""
            for (doc in cursor) {
                if(itr==0){
                    str = "[ "+ str + gson.toJson(doc)
                }else{
                    str = str +","+ gson.toJson(doc)
                }
                itr++
            }
            results = str+ " ]"
            res.status(200)
            conn.close()
        }catch (e: ReqlError){
            res.status(500)
            e.printStackTrace()
            System.out.println("rethink DB Error")
        }catch (e:Exception){
            res.status(500)
            e.printStackTrace()
        }
        return parser.parse(results);
    });
}

fun convertToUnixDate(d:String):Long{
    val dateFormat = SimpleDateFormat("ddMMyyyy")
    val date:Date = dateFormat.parse(d)
    val unixTime = date.time / 1000
    return unixTime
}